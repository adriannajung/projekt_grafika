#ifndef REMOVELAYERCOMMAND_H
#define REMOVELAYERCOMMAND_H

#include <QUndoStack>

class layer;

class removeLayerCommand : public QUndoCommand
{
public:
    removeLayerCommand(layer * _layer, int index);

    virtual void redo();
    virtual void undo();

private:
    layer * mLayer;
    int mIndex;
};


#endif // REMOVELAYERCOMMAND_H
