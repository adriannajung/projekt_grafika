#include "changecontrastcommand.h"
#include "layer.h"

changeContrastCommand::changeContrastCommand(layer * _layer, double intensity) :
    mLayer(_layer),
    mCopy(_layer->mImage.copy()),
    mIntensity(intensity)
{
}

void changeContrastCommand::redo()
{
    mLayer->mImage = mCopy;

    QRgb *st = (QRgb *) mLayer->mImage.bits();
    quint64 pixelCount = mLayer->mImage.width() * mLayer->mImage.height();

    for (quint64 p = 0; p < pixelCount; p++) {
        // st[p] has an individual pixel
        auto color = QColor(st[p]);
        int red = color.red() - 2.55 * mIntensity;
        red = (red > 255) ? 255 : red;
        red = (red < 0) ? 0 : red;
        int green = color.green() - 2.55 * mIntensity;
        green = (green > 255) ? 255 : green;
        green = (green < 0) ? 0 : green;
        int blue = color.blue() - 2.55 * mIntensity;
        blue = (blue > 255) ? 255 : blue;
        blue = (blue < 0) ? 0 : blue;
        st[p] = QColor(red, green, blue).rgb();
    }

    mLayer->refresh();
}

void changeContrastCommand::undo()
{
    mLayer->mImage = mCopy;
    mLayer->refresh();
}

