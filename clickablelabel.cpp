#include "clickablelabel.h"

ClickableLabel::ClickableLabel(int index, QWidget* parent, Qt::WindowFlags f)
    : labelIndex(index),
      QLabel(parent) {

}

ClickableLabel::~ClickableLabel() {}

void ClickableLabel::mousePressEvent(QMouseEvent* event) {
    emit clicked(labelIndex);
}
