#ifndef EDITFORM_H
#define EDITFORM_H

#include <QMouseEvent>
#include <QWidget>
#include "activeproject.h"

namespace Ui {
class editForm;
}

class editForm : public QWidget
{
    Q_OBJECT

public:
    explicit editForm(QWidget *parent, activeProject * project);
    ~editForm();

protected:
    bool eventFilter(QObject *obj, QEvent *event);

protected:
    void paintEvent(QPaintEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;

private:
    Ui::editForm * ui;
    activeProject * mProject;
};

#endif // EDITFORM_H
