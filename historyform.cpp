#include "historyform.h"
#include "ui_historyform.h"

historyForm::historyForm(QWidget *parent, activeProject * project) :
    QWidget(parent),
    ui(new Ui::historyForm),
    mProject(project),
    mLayout(new QVBoxLayout())
{
    ui->setupUi(this);
    ui->gridLayout->setAlignment(Qt::AlignCenter);
}


historyForm::~historyForm()
{
    delete ui;
}

void historyForm::closeEvent(QCloseEvent * event)
{
    emit this->windowClosed();
}

void historyForm::resizeEvent(QResizeEvent * event)
{
    QSize currentSize = this->size();
    QSize oldSize = event->oldSize();

    int heightScale = currentSize.height() / oldSize.height();
    int widthScale = currentSize.width() / oldSize.width();

    int widgetHeight = ui->scrollArea->height() * heightScale;
    int widgetWidth = ui->scrollArea->width() * widthScale;

    ui->scrollArea->resize(widgetHeight, widgetWidth);
}

 void historyForm::goToHistoryFromMiniature(int index)
 {
     int val = mProject->mUndoStack.count() - index;
     mProject->mUndoStack.setIndex(val);
 }

 ClickableLabel* historyForm::saveSceneAsLabel(QGraphicsScene * mScene, int miniatureIndex)
{
    QGraphicsView * gv = new QGraphicsView(mScene);
    QPixmap pixmap = gv->grab();

    ClickableLabel * label = new ClickableLabel(miniatureIndex, this);
    label->setPixmap(pixmap.scaled(160,160, Qt::KeepAspectRatio));

    return label;
}

void historyForm::createWidgetsMiniatures()
{
    int topIndex = mProject->mUndoStack.count() - 1;
    int currentIndex = mProject->mUndoStack.index();
    int copyIndex = topIndex;
    int miniatureIndex = 0;
    int row = 0;
    int column = 0;

    mProject->mUndoStack.setIndex(topIndex + 1);

    while (copyIndex >= 0){

        ClickableLabel * label = saveSceneAsLabel(mProject->mLayersManager.mScene, miniatureIndex);
        QObject::connect(label, SIGNAL(clicked(int)), this, SLOT(goToHistoryFromMiniature(int)));

        ui->gridLayout->addWidget(label, row, column++);

        if (column % 5 == 0) {
            column = 0;
            row++;
        }

        copyIndex--;
        miniatureIndex++;
        mProject->mUndoStack.undo();
    }

    mProject->mUndoStack.setIndex(currentIndex);

    this->setLayout(ui->gridLayout);
}
