#include "changecontrastcommand.h"
#include "toolsform.h"
#include "ui_toolsform.h"

toolsForm::toolsForm(QWidget *parent, activeProject *project) :
    QWidget(parent),
    mProject(project),
    ui(new Ui::toolsForm)
{
    ui->setupUi(this);
}

toolsForm::~toolsForm()
{
    delete ui;
}

void toolsForm::on_size_valueChanged(int arg1)
{
     mProject->mLayersManager.mEditor.mBrushSize=arg1;
}

void toolsForm::on_R_valueChanged(double arg1)
{
     mProject->mLayersManager.mEditor.mRed=arg1;
}

void toolsForm::on_G_valueChanged(double arg1)
{
     mProject->mLayersManager.mEditor.mGreen=arg1;
}

void toolsForm::on_B_valueChanged(double arg1)
{
     mProject->mLayersManager.mEditor.mBlue=arg1;
}

void toolsForm::on_A_valueChanged(double arg1)
{
    mProject->mLayersManager.mEditor.mAlpha=arg1;
}

void toolsForm::on_comboBox_activated(const QString &arg1)
{
    mProject->mLayersManager.mEditor.mBrushType = arg1;
}

void toolsForm::on_applyPushButton_pressed()
{
    if (ui->Type->currentText() == "Contrast"){
        mProject->mUndoStack.push(new changeContrastCommand(mProject->mLayersManager.getActive(), ui->Intensity->value()));
    } else {

    }
}
