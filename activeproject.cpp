#include "activeproject.h"
#include <QDir>
#include <QFileDialog>
#include <QDebug>

activeProject::activeProject(QObject *parent, QRect size) : QObject(parent), mImageSize(size), mLayersManager(this, this), mModel(this)
{
    mProjectPath = QDir::homePath();
}

QString &activeProject::getSaveFile()
{
    return mProjectPath;
}

void activeProject::updateSaveFile(QWidget * parent)
{
    QString fileName = QFileDialog::getSaveFileName(parent, tr("Save File"),
                               mProjectPath,
                               tr("Lmao project (*.lmao)"));

    mProjectPath = fileName;
}

void activeProject::store()
{
    QFile file(mProjectPath);
    if (file.open(QIODevice::WriteOnly)){

        QDataStream fileStream(&file);

        fileStream << *this;
    }
}

void activeProject::load(QString filePath)
{
    mProjectPath = filePath;

    QFile file(mProjectPath);
    if (file.open(QIODevice::ReadWrite)){
        QDataStream fileStream(&file);

        fileStream >> *this;
    }
}

void activeProject::exportImage(QWidget * parent)
{
    QString fileName = QFileDialog::getSaveFileName(parent, tr("Save File"),
                               mProjectPath,
                               tr("Images (*.png)"));

    qDebug() << fileName << '\n';
}

void activeProject::importAsNewLayer(QString filePath)
{

}

QDataStream &operator<<(QDataStream &out, activeProject& project)
{
    out << project.mProjectPath << project.mModel.stringList() << project.mLayersManager;

    return out;
}


QDataStream &operator>>(QDataStream &in, activeProject& project)
{
    QStringList list;

    in >> project.mProjectPath >> list >> project.mLayersManager;

    project.mModel.setStringList(list);

    return in;
}
