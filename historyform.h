#ifndef HISTORYFORM_H
#define HISTORYFORM_H

#include "activeproject.h"
#include "clickablelabel.h"
#include <QWidget>
#include <QLayout>
#include <QPixmap>

namespace Ui {
class historyForm;
}

class historyForm : public QWidget
{
    Q_OBJECT

public:
    explicit historyForm(QWidget *parent, activeProject *project);
    ~historyForm();

    QVBoxLayout * mLayout;

    ClickableLabel* saveSceneAsLabel(QGraphicsScene * mScene, int miniatureIndex);
    void createWidgetsMiniatures();

private slots:
    void goToHistoryFromMiniature(int index);

signals:
    void windowClosed();

protected:
    void closeEvent(QCloseEvent *event);
    void resizeEvent(QResizeEvent *event);

private:
    Ui::historyForm *ui;
    activeProject * mProject;
};

#endif // HISTORYFORM_H
