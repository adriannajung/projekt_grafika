#include "layereditor.h"
#include "activeproject.h"
#include <QPainter>
#include <QDebug>

layerEditor::layerEditor(QObject *parent, activeProject * project) : QObject(parent), mProject(project), mCurrentCommand(nullptr), mAlpha(100), mBrushSize(5), mBrushType("Circle")
{

}

void layerEditor::mouseDrawEvent(QMouseEvent *event)
{

    if (event->buttons() & Qt::MouseButton::LeftButton){
        auto _layer = mProject->mLayersManager.getActive();

        auto point = event->pos();

        if (_layer && point.x() > 0 && point.y() > 0 ){
            if (!mCurrentCommand){
                auto color = QColor(mRed * 2.55, mGreen* 2.55, mBlue* 2.55, mAlpha* 2.55);
                mCurrentCommand = new drawCommand(_layer, mBrushType, mBrushSize, color);
            }

            mCurrentCommand->addPoint(point);
        }
    } else if (mCurrentCommand){

        mProject->mUndoStack.push(mCurrentCommand);

        mCurrentCommand = nullptr;
    }

}
