#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMdiSubWindow>
#include <QPointer>
#include "activeproject.h"

namespace Ui {
class MainWindow;
}

class editForm;
class historyForm;
class toolsForm;
class layersForm;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionImport_triggered();

    void on_actionExport_triggered();

    void on_actionSave_triggered();

    void on_actionSave_As_triggered();

    void on_actionLoad_from_lmao_triggered();

    void on_actionUndo_triggered();

    void on_actionRedo_triggered();

    void on_actionShow_Edit_triggered();

    void on_actionShow_History_2_triggered();

    void on_actionShow_Tools_triggered();

    void on_actionShow_Layers_triggered();

    void onHistoryWindowClose();

private:
    Ui::MainWindow *ui;

    activeProject mCurrentlyActive;

    QPointer<editForm> mEditForm;
    QPointer<historyForm> mHistoryForm;
    QPointer<toolsForm> mToolsForm;
    QPointer<layersForm> mLayersForm;
    QMdiSubWindow * mEditWindow;
    QMdiSubWindow * mHistoryWindow;
    QMdiSubWindow * mToolsWindow;
    QMdiSubWindow * mLayersWindow;

    QMdiSubWindow * loadSubWindow(QWidget *widget);
};

#endif // MAINWINDOW_H
