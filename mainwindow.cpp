#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "editform.h"
#include "historyform.h"
#include "toolsform.h"
#include "layersform.h"
#include "activeproject.h"
#include <QMdiSubWindow>
#include <QFileDialog>
#include <QDebug>
#include <QFileInfo>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setCentralWidget(ui->mdiArea);
    on_actionShow_Tools_triggered();
    on_actionShow_Edit_triggered();
    on_actionShow_Layers_triggered();

    QObject::connect(mToolsForm, SIGNAL(brushSizeChanged(int)), mLayersForm, SLOT(setBrushSize(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionImport_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    mCurrentlyActive.getSaveFile(),
                                                    tr("Images (*.png *.xpm *.jpg)"));

    auto _layer = mCurrentlyActive.mLayersManager.addLayer(0);
    mCurrentlyActive.mModel.setData(mCurrentlyActive.mModel.index(0,0),QString("Image"));

    _layer->loadImage(fileName);
}

void MainWindow::on_actionExport_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                               QDir::homePath(),
                               tr("Images (*.png)"));

    QImage image = QImage(mCurrentlyActive.mImageSize.width(), mCurrentlyActive.mImageSize.height(), QImage::Format_ARGB32);
    QPainter painter(&image);
    //painter.setRenderHint(QPainter::Antialiasing);
    mCurrentlyActive.mLayersManager.mScene->render(&painter);
    image.save(fileName);
}

void MainWindow::on_actionSave_triggered()
{
    QFileInfo fileInfo(mCurrentlyActive.getSaveFile());

    if (!fileInfo.isFile()){
        mCurrentlyActive.updateSaveFile(this);
    }

    mCurrentlyActive.store();
}

void MainWindow::on_actionSave_As_triggered()
{
    mCurrentlyActive.updateSaveFile(this);
    mCurrentlyActive.store();
}


void MainWindow::on_actionLoad_from_lmao_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                               QDir::homePath(),
                               tr("Lmao project (*.lmao)"));

    mCurrentlyActive.load(fileName);
}

void MainWindow::on_actionUndo_triggered()
{
    mCurrentlyActive.mUndoStack.undo();
}

void MainWindow::on_actionRedo_triggered()
{
    mCurrentlyActive.mUndoStack.redo();
}

void MainWindow::on_actionShow_Edit_triggered()
{
    if (!mEditForm.data()){
        mEditForm = new editForm(this, &mCurrentlyActive);
        mEditWindow = loadSubWindow(mEditForm);
        mEditWindow->setFixedSize(810, 630);
    }

    mEditWindow->show();
}

void MainWindow::on_actionShow_History_2_triggered()
{
    if (!mHistoryForm.data()){
        mHistoryForm = new historyForm(this, &mCurrentlyActive);
        QObject::connect(mHistoryForm, SIGNAL(windowClosed()), this, SLOT(onHistoryWindowClose()));

        mHistoryForm->createWidgetsMiniatures();

        mHistoryWindow = loadSubWindow(mHistoryForm);
    }

    mHistoryWindow->show();

    if (mEditWindow->isVisible())
        mEditWindow->setEnabled(false);

    if (mLayersWindow->isVisible())
        mLayersWindow->setEnabled(false);

    if (mToolsWindow->isVisible())
        mToolsWindow->setEnabled(false);
}

void MainWindow::on_actionShow_Tools_triggered()
{
    if (!mToolsForm.data()){
        mToolsForm = new toolsForm(this,&mCurrentlyActive);
        mToolsWindow = loadSubWindow(mToolsForm);
    }

    mToolsWindow->show();
}

QMdiSubWindow * MainWindow::loadSubWindow(QWidget *widget)
{
    auto window = ui->mdiArea->addSubWindow(widget);

    window->setWindowTitle(widget->windowTitle());
    window->setWindowIcon(widget->windowIcon());

    return window;
}

void MainWindow::on_actionShow_Layers_triggered()
{
    if (!mLayersForm.data()){
        mLayersForm = new layersForm(this, &mCurrentlyActive);
        mLayersWindow = loadSubWindow(mLayersForm);
    }

    mLayersWindow->show();
}

void MainWindow::onHistoryWindowClose()
{
    if (mEditWindow->isVisible())
        mEditWindow->setEnabled(true);

    if (mLayersWindow->isVisible())
        mLayersWindow->setEnabled(true);

    if (mToolsWindow->isVisible())
        mToolsWindow->setEnabled(true);
}


