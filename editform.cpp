#include "editform.h"
#include "ui_editform.h"

#include <QPainter>
#include <QDebug>
#include <QCoreApplication>
#include <QEvent>
#include <QMdiSubWindow>

editForm::editForm(QWidget *parent, activeProject * project) :
    QWidget(parent),
    ui(new Ui::editForm),
    mProject(project)
{
    ui->setupUi(this);
    ui->graphicsView->setScene(mProject->mLayersManager.mScene);
    QCoreApplication::instance()->installEventFilter(this);
}

editForm::~editForm()
{
    delete ui;
}

bool editForm::eventFilter(QObject *obj, QEvent *event)
{
    // Dirty hack

    auto isMdi = dynamic_cast<QMdiSubWindow*>(obj);

    if (this->isEnabled())
    {
        if(event->type() == QEvent::MouseMove && obj->objectName() == "" && !isMdi){
            //qDebug() << obj;
            mouseMoveEvent(static_cast<QMouseEvent *>(event));
        } else if (event->type() == QEvent::Paint){
            paintEvent(static_cast<QPaintEvent *>(event));
        }
    }


    return false;
}

void editForm::paintEvent(QPaintEvent * /* event */)
{
    mProject->mLayersManager.draw();
}

void editForm::mouseMoveEvent(QMouseEvent *event)
{
    mProject->mLayersManager.mEditor.mouseDrawEvent(event);
}
