#ifndef ACTIVEPROJECT_H
#define ACTIVEPROJECT_H

#include "layersmanager.h"

#include <QObject>
#include <QUndoStack>
#include <QWidget>
#include <QRect>
#include <QStringListModel>
#include "serializableclass.h"
#include <QtScript/qscriptvalue.h>

class activeProject : public QObject
{
    Q_OBJECT

public:
    explicit activeProject(QObject *parent = 0, QRect size = QRect(0, 0, 800, 600));

    QString & getSaveFile();
    void updateSaveFile(QWidget * parent);

    void store();
    void load(QString filePath);

    void exportImage(QWidget * parent);
    void importAsNewLayer(QString filePath);

    QUndoStack mUndoStack;

    layersManager mLayersManager;

    QRect mImageSize;

    QStringListModel mModel;

signals:

public slots:

private:
    QString mProjectPath;

    friend QDataStream & operator<<(QDataStream &out, activeProject& project);
    friend QDataStream & operator>>(QDataStream &in, activeProject& project);
};

QDataStream & operator<<(QDataStream &out, activeProject& project);
QDataStream & operator>>(QDataStream &in, activeProject& project);


#endif // ACTIVEPROJECT_H
