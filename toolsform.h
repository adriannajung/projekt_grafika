#ifndef TOOLSFORM_H
#define TOOLSFORM_H

#include <QWidget>
#include "activeproject.h"

namespace Ui {
class toolsForm;
}

class toolsForm : public QWidget
{
    Q_OBJECT

public:
    explicit toolsForm(QWidget *parent, activeProject *project);
    ~toolsForm();

public slots:

    void on_size_valueChanged(int arg1);

    void on_R_valueChanged(double arg1);

    void on_G_valueChanged(double arg1);

    void on_B_valueChanged(double arg1);

    void on_A_valueChanged(double arg1);

    void on_comboBox_activated(const QString &arg1);

private slots:

    void on_applyPushButton_pressed();

private:
    Ui::toolsForm *ui;
    activeProject *mProject;
};

#endif // TOOLSFORM_H
